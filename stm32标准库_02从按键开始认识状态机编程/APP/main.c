#include "includes.h"


int main(void)
{
	  SysTick_Init();  //时钟初始化
	  uart_init(115200);
	  LED_Init();      //LED初始化
	  KEY_Init();      //按键初始化
	  while(1)
		{
//			LED_Task();  			//LED测试任务
				Key_ScanTask();   //按键轮询状态机检测任务
			  if(g_Key.key_click[KEY_CH1] == true)
				{		
					g_Key.key_click[KEY_CH1] = false;
					printf("按键1按下,LED1,2,3点亮");
				  LED1_ON;
					LED2_ON;
					LED3_ON;
				}
				if(g_Key.key_click[KEY_CH2] == true)
				{		
					g_Key.key_click[KEY_CH2] = false;
					printf("按键2按下,LED1,2,3熄灭");
				  LED1_OFF;
					LED2_OFF;
					LED3_OFF;
				}
				if(g_Key.key_longpress[KEY_CH3] == true)
				{		
					g_Key.key_longpress[KEY_CH3] = false;
					printf("按键3长按,LED1,2,3翻转");
				  LED1_TOGGLE;
					LED2_TOGGLE;
					LED3_TOGGLE;
				}
		}
}
