#ifndef __TIMER_H__
#define __TIMER_H__

#include "stm32f4xx.h"
#include "usart.h"



/**********外部函数调用开始*************/
//定时器3初始化 参数：1.自动重装载值 2.预分频系数
void TIM3_Int_Init(u16 reload ,u16 prescaler);  

/**********外部函数调用结束*************/
#endif
