#include "timer.h"
#include "Dbg.h"
#include <stdio.h>
/*
*********************************************************************************************************
*	函 数 名: timer3_NvicConfig
*	功能说明: 串口1NVIC设置
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/ 
void timer3_NvicConfig(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	
	//Timer3 NVIC 配置
	NVIC_InitStructure.NVIC_IRQChannel=TIM3_IRQn; 					//定时器3中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2; //抢占优先级2
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=0; 				//无子优先级
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
}

/*
*********************************************************************************************************
*	函 数 名: TIM3_Int_Init
*	功能说明: 定时器初始化函数
*	形    参: 1.自动重装值  2.时钟预分频数
*	返 回 值: 无
*********************************************************************************************************
*/ 
void TIM3_Int_Init(u16 reload ,u16 prescaler)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);  ///使能TIM3时钟
	
  TIM_TimeBaseInitStructure.TIM_Period = reload; 	     //自动重装载值
	TIM_TimeBaseInitStructure.TIM_Prescaler=prescaler;   //定时器分频
	TIM_TimeBaseInitStructure.TIM_CounterMode=TIM_CounterMode_Up; //向上计数模式
	TIM_TimeBaseInitStructure.TIM_ClockDivision=TIM_CKD_DIV1; 
	
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStructure);//初始化TIM3
	
//	TIM_ClearFlag(TIM3,TIM_FLAG_Update);     //清除定时器3中断标志
	TIM_ClearITPendingBit(TIM3,TIM_IT_Update);  //清除中断标志位
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE); //允许定时器3更新中断
	TIM_Cmd(TIM3,ENABLE); 	//使能定时器3
	
  timer3_NvicConfig();		//NVIC配置
	
	
}

/*
*********************************************************************************************************
*	函 数 名: TIM3_IRQHandler
*	功能说明: 定时器3中断服务函数
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/ 
void TIM3_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM3,TIM_IT_Update)==SET) //溢出中断
	{
		 UART1_PackageISR();
	}
	TIM_ClearITPendingBit(TIM3,TIM_IT_Update);  //清除中断标志位
}


