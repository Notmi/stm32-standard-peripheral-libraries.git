#include "usart.h"
#include "String.h"

UARTx_T g_Uart1 = {0};

/*
*********************************************************************************************************
*	函 数 名: uart1_NvicConfig
*	功能说明: 串口1NVIC设置
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/ 
void uart1_NvicConfig(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	
	//Usart1 NVIC 配置
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;       //串口1中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3; //抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority =0;		    //分组4下无子优先级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			    //IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器、
	
}

/*
*********************************************************************************************************
*	函 数 名: uart1_Init
*	功能说明: 串口1初始化函数
*	形    参: 1.bound（串口波特率）
*	返 回 值: 无
*********************************************************************************************************
*/ 
void uart1_Init(u32 bound)
{
   //GPIO端口设置
  GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE); //使能GPIOA时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);//使能USART1时钟
 
	//串口1对应引脚复用映射
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource9,GPIO_AF_USART1); //GPIOA9复用为USART1
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource10,GPIO_AF_USART1); //GPIOA10复用为USART1
	
	//USART1端口配置
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10; //GPIOA9与GPIOA10
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;//复用功能
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;	//速度50MHz
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; //推挽复用输出
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP; //上拉
	GPIO_Init(GPIOA,&GPIO_InitStructure); //初始化PA9，PA10

   //USART1 初始化设置
	USART_InitStructure.USART_BaudRate = bound;//波特率设置
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode =  USART_Mode_Rx | USART_Mode_Tx;	//收发模式
  USART_Init(USART1, &USART_InitStructure); //初始化串口1
	
	uart1_NvicConfig();  //NVIC配置
	
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);  //开启串口接收中断
//	USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);  //开启串口空闲中断
	
  USART_Cmd(USART1, ENABLE);  //使能串口1 
	//USART_ClearFlag(USART1, USART_FLAG_TC);   //解决第一个字节丢失问题,如果第一个字节丢失可以加上这行
	
	/***** 1s有10000个100us	RECV_TIMEROUT_BYTE为10个Byte  ******/	
	g_Uart1.RecvPackageTimeOut = 10*RECV_TIMEROUT_BYTE*10000/bound;  //获取超时时间值
	Println("超时时间值为%d个100us",g_Uart1.RecvPackageTimeOut);	
	
}


//加入以下代码,支持printf函数,而不需要选择use MicroLIB	
#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE
{ 
	int handle; 
}; 

FILE __stdout;  

//定义_sys_exit()以避免使用半主机模式    
void _sys_exit(int x) 
{ 
	x = x; 
} 

/*
*********************************************************************************************************
*	函 数 名: fputc
*	功能说明: 重定义fputc函数
*	形    参: 1:ch(待写入字符) 2:f(文件指针)
*	返 回 值: 无
*********************************************************************************************************
*/ 
#define USE_REGISTER      //是否使用寄存器操作

int fputc(int ch, FILE *f)
{ 	

#ifdef 	USE_REGISTER      //使用寄存器操作

	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
	USART1->DR = (u8) ch;  
	
#else	                    //使用库函数操作
	
	USART_SendData(USART1, (unsigned char)ch);
  while (!USART_GetFlagStatus(USART1, USART_FLAG_TC));
  USART_ClearFlag(USART1, USART_FLAG_TC);
	
#endif
	
	return ch;
}

/*
*********************************************************************************************************
*	函 数 名: uart1_SendBuff
*	功能说明: 串口1发送函数
*	形    参: 要发送的数据  和  数据长度
*	返 回 值: 无
*********************************************************************************************************
*/
void uart1_SendBuff(u8 * buf, u16 len)
{
	while(len--)
	{
		while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET); //等待发送结束
		USART_SendData(USART1, *buf++);
	}
	while(USART_GetFlagStatus(USART1,USART_FLAG_TC)==RESET);
}


/*
*********************************************************************************************************
*	函 数 名: USART1_IRQHandler
*	功能说明: 串口1接收中断服务函数
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/ 
#define    USART_MAX_LEN    150            //最大接收长度 150个字节
u8  USART1_RecvBuff[USART_MAX_LEN] = {0};  //接收buff
u16 RecvBuffLen = 0;          						 //接收长度
void USART1_IRQHandler(void)  //串口1中断服务程序
{
	if(USART_GetITStatus(USART1,USART_IT_RXNE) != RESET) //接收中断触发
	{		
			if(RecvBuffLen < USART_MAX_LEN)
			{
				 //读DR把数据一个字节一个字节读出，运行这个会自动清除中断标志位，	  
			    USART1_RecvBuff[RecvBuffLen++] = USART_ReceiveData(USART1); 
          g_Uart1.RecvFlag = true;          //串口接收到数据标志
		      g_Uart1.RecvTimerOutTick = 0;			//串口超时计数清空
				
			}
			else 	 //一次性接收大于150个字节，数组从头开始接收
			{
			   USART_ReceiveData(USART1);              //清除空闲标志
			   RecvBuffLen = 0;
			}
	}	
	
//	if(USART_GetITStatus(USART1, USART_IT_IDLE) != RESET) //空闲中断触发
//	{	
//		 USART_ReceiveData(USART1);                //清除空闲标志		
//     if(RecvBuffLen != 0)                      //表示接收到数据
//		 {
//		    uart1_SendBuff(USART1_RecvBuff,RecvBuffLen); //把收到的数据发出来
//		    RecvBuffLen = 0;	
//		 }		      
//	} 
}


/*
*********************************************************************************************************
*	函 数 名: UART1_PackageISR
*	功能说明: 串口1超时判断函数
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void UART1_PackageISR(void)
{
    g_Uart1.RecvTimerOutTick++;  //超时Tick不断
																 //接收到数据 同时 达到超时分包
    if((g_Uart1.RecvFlag == true) && (g_Uart1.RecvTimerOutTick > g_Uart1.RecvPackageTimeOut)) 
    {
        g_Uart1.RecvFlag = false;        //串口接收到数据标志清空
        g_Uart1.RecvTimerOutTick = 0;    //超时Tick清空
        g_Uart1.RecvPackageFlag = true;  //成功接收到一包数据
			  
    }
}

/*
*********************************************************************************************************
*	函 数 名: UART1_Task
*	功能说明: 串口1接收处理函数
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void UART1_Task(void)
{
    if(g_Uart1.RecvPackageFlag)
    {
       g_Uart1.RecvPackageFlag = false;
			 
			 g_Uart1.RecvSucLen =  RecvBuffLen;  //获取数据长度	
			 memcpy(g_Uart1.RecvBuffer,USART1_RecvBuff,RecvBuffLen);
			 RecvBuffLen = 0;
			 uart1_SendBuff(g_Uart1.RecvBuffer,g_Uart1.RecvSucLen);   //接收到什么打印什么
    }
    
}

