#ifndef __USART_H
#define __USART_H

#include "includes.h"


#define  UARTx_BUFFER_SIZE         256  //串口接收BUFF大小
#define  RECV_TIMEROUT_BYTE        10   //串口接收超时字节 

/*
**********************************************************************************************************
											串口结构体
**********************************************************************************************************
*/
typedef struct
{
		u8   RecvSucLen;                       	   //接收数据成功长度
    u8   RecvBuffer[UARTx_BUFFER_SIZE];        //接收缓冲区
    u8   RecvFlag;                             //正在接收标志
    u8   RecvPackageFlag;					  					 //接收成功一包标志
		u8   RecvTimerOutTick;					  				 //接收超时时间计时
    u8   RecvPackageTimeOut;                   //接收超时时间
}UARTx_T;

/**********外部函数调用开始*************/
void uart1_Init(u32 bound); 		//串口1初始化函数

void UART1_PackageISR(void);    //串口超时检测分包函数
void UART1_Task(void);          //串口1接收处理函数
/**********外部函数调用结束*************/


#endif
