#include "LED.h"

/*
*********************************************************************************************************
*	函 数 名: LED_Init
*	功能说明: LED初始化
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/ 
void LED_Init(void)
{

	  GPIO_InitTypeDef  GPIO_InitStructure;
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD, ENABLE);

		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_2;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_Init(GPIOD, &GPIO_InitStructure);
	
	  GPIO_SetBits(GPIOC,GPIO_Pin_0);
	  GPIO_SetBits(GPIOC,GPIO_Pin_2);
		GPIO_SetBits(GPIOD,GPIO_Pin_3);
}

/*
*********************************************************************************************************
*	函 数 名: LED_Task
*	功能说明: LED任务
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/ 
static u32 LedTask_Tick;

void LED_Task(void)
{
		if(SysTick_1ms - LedTask_Tick >= 1000)  //一秒进来一次
		{ 
		   LedTask_Tick = SysTick_1ms; //更新时间
			 LED1_TOGGLE;
			 LED2_TOGGLE;
			 LED3_TOGGLE;
			 printf("一秒打印一次\r\n");
		}
}










