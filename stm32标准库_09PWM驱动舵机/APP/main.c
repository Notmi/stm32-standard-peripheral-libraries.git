#include "includes.h"


int main(void)
{
	  SysTick_Init();  //时钟初始化
	  TIM4_Int_Init(999,1679); //定时器初始化
	  uart1_Init(115200);
	  LED_Init();      //LED初始化
	  KEY_Init();      //按键初始化
	  Printf(USART1,"初始化完成,打印开关为%d\r\n",Print_Switch);
	  while(1)
		{
//			LED_Task();  			//LED测试任务
			uart1_process();
			Key_ScanTask();   //按键轮询状态机检测任务
			if(g_Key.key_click[KEY_CH1] == true)
			{		
				g_Key.key_click[KEY_CH1] = false;
				Printf(USART1,"按键1按下,舵机转至0度\r\n");
				Set_Servo_Angle(0);
			}
			else if(g_Key.key_click[KEY_CH2] == true)
			{		
				g_Key.key_click[KEY_CH2] = false;
				Printf(USART1,"按键2按下,舵机转至90度\r\n");
				Set_Servo_Angle(90);
			}
			else if(g_Key.key_click[KEY_CH3] == true)
			{		
				g_Key.key_click[KEY_CH3] = false;
				Printf(USART1,"按键3按下,舵机转至180度\r\n");
				Set_Servo_Angle(180);
			}			
		}
}
