#include "includes.h"


int main(void)
{
	  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);//设置系统中断优先级分组4
	
	  SysTick_Init();  //时钟初始化
	
	  uart1_Init(1500000);
	  LED_Init();      //LED初始化
	  KEY_Init();      //按键初始化
	  Println("初始化完成,打印开关为%d",Print_Switch);
	  while(1)
		{
//			LED_Task();  			//LED测试任务
				Key_ScanTask();   //按键轮询状态机检测任务
			  if(g_Key.key_click[KEY_CH1] == true)
				{		
					g_Key.key_click[KEY_CH1] = false;
					Println("按键1按下,LED1,2,3翻转");
				  LED1_TOGGLE;
					LED2_TOGGLE;
					LED3_TOGGLE;
				}
		}
}
