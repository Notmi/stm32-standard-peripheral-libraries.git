#include "usart.h"

//函数声明
void USART1_DMA_Rx_Config(void);
#define    USART_MAX_LEN    150            //最大接收长度 150个字节
u8  USART1_RecvBuff[USART_MAX_LEN] = {0};  //接收buff
u16 RecvBuffLen = 0;          						 //接收长度

/*
*********************************************************************************************************
*	函 数 名: uart1_NvicConfig
*	功能说明: 串口1NVIC设置
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/ 
void uart1_NvicConfig(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	
	//Usart1 NVIC 配置
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;       //串口1中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1; //抢占优先级1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority =0;		    //分组4下无子优先级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			    //IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器、
	
}



/*
*********************************************************************************************************
*	函 数 名: uart1_Init
*	功能说明: 串口1初始化函数
*	形    参: 1.bound（串口波特率）
*	返 回 值: 无
*********************************************************************************************************
*/ 
void uart1_Init(u32 bound)
{
   //GPIO端口设置
  GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE); //使能GPIOA时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);//使能USART1时钟
 
	//串口1对应引脚复用映射
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource9,GPIO_AF_USART1); //GPIOA9复用为USART1
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource10,GPIO_AF_USART1); //GPIOA10复用为USART1
	
	//USART1端口配置
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10; //GPIOA9与GPIOA10
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;//复用功能
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;	//速度100MHz
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; //推挽复用输出
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP; //上拉
	GPIO_Init(GPIOA,&GPIO_InitStructure); //初始化PA9，PA10

   //USART1 初始化设置
	USART_InitStructure.USART_BaudRate = bound;//波特率设置
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode =  USART_Mode_Rx | USART_Mode_Tx;	//收发模式
  USART_Init(USART1, &USART_InitStructure); //初始化串口1
	
	uart1_NvicConfig();  //NVIC配置
	
//	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);  //开启串口接收中断
	USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);  //开启串口空闲中断
	USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE);  // 开启串口DMA接收
	USART1_DMA_Rx_Config();                         // 配置串口DMA接收
	
  USART_Cmd(USART1, ENABLE);  //使能串口1 
	//USART_ClearFlag(USART1, USART_FLAG_TC);   //解决第一个字节丢失问题,如果第一个字节丢失可以加上这行
}

/*
*********************************************************************************************************
*	函 数 名: USART1_DMA_Rx_Config
*	功能说明: 串口1DMA接收配置函数
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/ 
void USART1_DMA_Rx_Config(void)
{
	DMA_InitTypeDef DMA_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);  	// 开启DMA时钟

	DMA_InitStructure.DMA_Channel = DMA_Channel_4; 													 //通道选择
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USART1->DR;				 //DMA外设地址
  DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)USART1_RecvBuff;			 //DMA 存储器0地址
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;   							 //存储器到外设模式
  DMA_InitStructure.DMA_BufferSize = USART_MAX_LEN;												 //数据传输量 
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;				 //外设非增量模式
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;									 //存储器增量模式
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;  //外设数据长度:8位
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;					 //存储器数据长度:8位
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;												     //使用普通模式 
  DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;									 //高等优先级
	
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;                   //不开启FIFO模式
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;            //FIFO阈值
	
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;								//存储器突发单次传输
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;				//外设突发单次传输
	
  DMA_Init(DMA2_Stream5, &DMA_InitStructure);																//初始化DMA Stream	
}


//加入以下代码,支持printf函数,而不需要选择use MicroLIB	
#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE
{ 
	int handle; 
}; 

FILE __stdout;  

//定义_sys_exit()以避免使用半主机模式    
void _sys_exit(int x) 
{ 
	x = x; 
} 

/*
*********************************************************************************************************
*	函 数 名: fputc
*	功能说明: 重定义fputc函数
*	形    参: 1:ch(待写入字符) 2:f(文件指针)
*	返 回 值: 无
*********************************************************************************************************
*/ 
#define USE_REGISTER      //是否使用寄存器操作

int fputc(int ch, FILE *f)
{ 	

#ifdef 	USE_REGISTER      //使用寄存器操作

	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
	USART1->DR = (u8) ch;  
	
#else	                    //使用库函数操作
	
	USART_SendData(USART1, (unsigned char)ch);
  while (!USART_GetFlagStatus(USART1, USART_FLAG_TC));
  USART_ClearFlag(USART1, USART_FLAG_TC);
	
#endif
	
	return ch;
}

/*
*********************************************************************************************************
*	函 数 名: uart1_SendBuff
*	功能说明: 串口1发送函数
*	形    参: 要发送的数据  和  数据长度
*	返 回 值: 无
*********************************************************************************************************
*/
void uart1_SendBuff(u8 * buf, u16 len)
{
	while(len--)
	{
		while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET); //等待发送结束
		USART_SendData(USART1, *buf++);
	}
	while(USART_GetFlagStatus(USART1,USART_FLAG_TC)==RESET);
}

/*
*********************************************************************************************************
*	函 数 名: Receive_DataPack
*	功能说明: 从DMA接收buff中取出接收到的数据
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/ 
void Receive_DataPack(void)
{

	uint32_t buff_length;            						 /* 接收的数据长度 */
	DMA_Cmd(DMA2_Stream5, DISABLE);  					   /* 暂时关闭dma，数据尚未处理 */ 
	buff_length = USART_MAX_LEN - DMA_GetCurrDataCounter(DMA2_Stream5);/* 获取接收到的数据长度 单位为字节*/
	RecvBuffLen = buff_length;         					 /* 获取数据长度 */
	DMA_ClearFlag(DMA2_Stream5,DMA_FLAG_TCIF5);  /* 清DMA标志位 */         
	DMA_SetCurrDataCounter(DMA2_Stream5,USART_MAX_LEN);/* 重新赋值计数值，必须大于等于最大可能接收到的数据帧数目 */
	
	uart1_SendBuff(USART1_RecvBuff,RecvBuffLen); /*打印接收到的数据*/
	
	DMA_Cmd(DMA2_Stream5, ENABLE);      				  /*打开DMA*/
}
/*
*********************************************************************************************************
*	函 数 名: USART1_IRQHandler
*	功能说明: 串口1接收中断服务函数
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/ 
void USART1_IRQHandler(void)  //串口1中断服务程序
{
	/* 使用串口DMA */
	if(USART_GetITStatus(USART1,USART_IT_IDLE)!=RESET) //空闲中断触发
	{		
		Receive_DataPack();                              //取出接收数据包并处理函数
		USART_ReceiveData(USART1);   //清除空闲中断标志位（接收函数有清标志位的作用）
	}	
}

