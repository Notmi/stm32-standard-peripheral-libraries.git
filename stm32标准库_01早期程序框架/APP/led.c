#include "LED.h"
#include "systick.h"
/*
*********************************************************************************************************
*	函 数 名: LED_Init
*	功能说明: LED初始化
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/ 
void LED_Init(void)
{
	 GPIO_InitTypeDef  GPIO_InitStructure;	
	 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);//使能PC端口时钟
	 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;				 //LED0-->PC.13 端口配置
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  //推挽输出
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //IO口速度为50MHz
	 GPIO_Init(GPIOC, &GPIO_InitStructure);					   //根据设定参数初始化GPIOC
	 GPIO_ResetBits(GPIOC,GPIO_Pin_13);						     //PC13 输出低电平
}

/*
*********************************************************************************************************
*	函 数 名: LED_Task
*	功能说明: LED任务
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/ 
static u32 LedTask_Tick;

void LED_Task(void)
{
		if(SysTick_1ms - LedTask_Tick > 1000)  //一秒进来一次
		{ 
		   LedTask_Tick = SysTick_1ms; //更新时间
			 LED0_TOGGLE;
		}
}


