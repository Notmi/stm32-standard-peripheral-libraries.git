#ifndef __LED_H
#define __LED_H	 

#include "stm32f10x.h"

#define GPIO_Toggle(port, bit) {    if(GPIO_ReadOutputDataBit(port,bit))   \
                                       GPIO_ResetBits(port, bit);          \
                                    else                                   \
                                        GPIO_SetBits(port, bit);           \
                               }

#define  LED0_OFF       GPIO_ResetBits(GPIOC,GPIO_Pin_13)
#define  LED0_ON        GPIO_SetBits(GPIOC,GPIO_Pin_13)    
#define  LED0_TOGGLE    GPIO_Toggle(GPIOC,GPIO_Pin_13)                               

void LED_Init(void);//��ʼ��
void LED_Task(void);//LED����
															 
#endif























